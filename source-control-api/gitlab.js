const axios = require('axios');

class Gitlab {

    constructor() {
        this.accessToken = process.env.GITLAB_TOKEN || null;
        this.instance = axios.create({
            baseURL: 'https://gitlab.com/api/v4',
            timeout: 12000,
            headers: { 'PRIVATE-TOKEN': this.accessToken }
        });
        this.project = null;
    }

    async setup(repo) {
        // get the repo name from the URL, remove .git
        const path = repo.split('/');
        const term = path[path.length - 1].replace('.git', '');
        const projects = await this.instance.get(`/projects?membership=true&search=${term}`);
        if (projects.data.length > 1) {
            for (let repo of projects.data) {
                if (repo.ssh_url_to_repo === repo || repo.http_url_to_repo === repo) {
                    this.project = repo;
                }
            }
        } else if (projects.data.length === 1) {
            this.project = projects.data[0];
        }

        if (!this.project) {
            throw new Error('Repo not found in gitlab API, do you have permission to it?');
        }
    }

    async openMergeRequest(fromBranch, toBranch, title, projectId) {
        return this.instance.post(`/projects/${projectId || this.project.id}/merge_requests`, {
            source_branch: fromBranch,
            target_branch: toBranch,
            title
        });
    }


}

module.exports = Gitlab;