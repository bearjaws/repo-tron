#!/usr/bin/env node
const yargs = require('yargs/yargs');
const { hideBin } = require('yargs/helpers')
const { exec } = require('child_process');
const fs = require('fs');
const tempDir = { cwd: './tmp' };
const GitlabApi = require('./source-control-api/gitlab');
const GitLab = new GitlabApi();
async function main(argv) {
    const parsed = yargs(hideBin(argv)).argv;
    const repoUrl = parsed['_'][0];
    const gl = await GitLab.setup(repoUrl);
    console.log(gl);

    try {
        fs.rmdirSync('./tmp', { recursive: true });
        const clone = await execPromise(`git clone --single-branch --branch ${parsed.branch || 'master'} ${repoUrl} ./tmp`);
        const type = await getRepoType();
        const fix = await fixDependencies(type, tempDir, parsed);
        console.log(fix);
        const branchName = getBranchName(parsed)
        const newBranchCommand = `git checkout -b ${branchName}`;
        await execPromise(newBranchCommand, tempDir);
        await execPromise(`git add .`, tempDir);
        const commit = await execPromise(`git commit -m "Ran audit and fixed dependencies"`, tempDir);
        if (!parsed.dryrun) {
            console.log('Not a dry run, pushing to repo and opening merge requests');
            const push = await execPromise(`git push origin ${branchName}`, tempDir);
            await GitLab.openMergeRequest(branchName, 'master', 'Repo-tron: Automatic dependency upgrade');
        }
    } catch (err) {
        console.log(err);
    }
}

main(process.argv);


/**
 * Runs exec in a promise, rejects if an error occurs, returns an object of stdout, stderr, error
 * 
 * @param {string} command 
 * @param {object} opts 
 */
function execPromise(command, opts) {
    return new Promise((resolve, reject) => {
        exec(command, opts, (error, stdout, stderr) => {
            if (!error) {
                return resolve({ stdout, stderr, error: null });
            }
            reject({ stdout, stderr, error });
        })
    })
}

/**
 * For now just makes a branch named repo-tron with a timestamp
 */
function getBranchName() {
    return `repo-tron-` + Math.ceil(new Date().getTime() / 1000);
}

/**
 * Introspects repo directory to see what dependency manager is in use
 */
async function getRepoType() {
    const dependencyConfigurations = {
        'npm': 'cat package.json'
    };
    for (let [type, command] of Object.entries(dependencyConfigurations)) {
        try {
            await execPromise(command, tempDir);
            return type;

        } catch (err) {
            console.log(err);
        }
    }

    throw new Error('Unknown dependency manager used in repo');
}

/**
 * Runs the command to upgrade depndencies
 * 
 * @param {string} type - Dependency manager type
 * @param {object} repoDir - An object describing the CWD for exec 
 * @param {object} argv - Parsed arguments from CLI.
 */
async function fixDependencies(type, repoDir, argv) {
    if (type === 'npm') {
        return await fixNpm(repoDir, argv);
    }
}

/**
 * Fixes NPM audit issues
 * 
 * @param {object} repoDir - An object describing the CWD for exec 
 * @param {object} argv - Parsed arguments from CLI.
 */
async function fixNpm(repoDir, argv) {
    let command = `npm audit fix`;
    if (argv.force === true) {
        command += ` --force`;
    }
    if (argv.exact === true) {
        command += ' --save-exact';
    }
    const audit = await execPromise(command, repoDir);
    const position = audit.stdout.indexOf('fixed ');
    return audit.stdout.slice(position);
}